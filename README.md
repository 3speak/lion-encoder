# lion-encoder

This is the enocding software written in VB.Net and used by 3Speak to process uploaded video files. 

### License

This software is licensed under the Creative Commons Attribution-NonCommercial 4.0 International Public License

### IDE

This software was created using the [Rider IDE by Jetbrains](https://www.jetbrains.com/rider/) on MacOS

### Contact

nico@3speak.online