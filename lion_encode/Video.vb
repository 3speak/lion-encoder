Imports System.Net

Public Class Video
    Public Sub Download(ByRef url as string, ByVal outputPath as String, ByVal filename as String)
        Console.WriteLine("> Start Download: " & url)
        if (Not System.IO.Directory.Exists(outputPath)) Then
            Console.WriteLine(outputPath)
            System.IO.Directory.CreateDirectory(outputPath)
        End If
        Dim web = New WebClient()
        web.DownloadFile(url, outputPath & "/" + filename)
    End Sub

End Class