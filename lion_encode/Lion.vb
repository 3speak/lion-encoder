﻿Imports System.IO
Imports Newtonsoft.Json.Linq

Module Lion
    Sub Main(args as String())
        
        Console.WriteLine("> Welcome to the lion encoder " + GetType(Lion).Assembly.GetName().Version.ToString())
        
        Dim config as Config = New Config(GetArg("config"))

        Dim ffmpeg as String = config.GetItem("ffmpeg")
        Dim hwaccel as String = config.GetItem("hwaccel")
        Dim apiHost as String = config.GetItem("jobUrl")

        if (apiHost.Length = 0 or not apiHost.StartsWith("http"))
            Console.WriteLine("> jobUrl is required")
            return
        End If

        Dim job as JObject = JSON.ParseUrl(apiHost)

        if (job.ContainsKey("error"))
            Console.WriteLine("ERROR: " & job.Item("error").ToString())
            Return
        End If

        if (Not job.ContainsKey("permlink")) Then
            Console.WriteLine("ERROR: received job does not include permlink.")
            return
        End If

        if (Not job.ContainsKey("video_url")) Then
            Console.WriteLine("ERROR: received job does not include video_url.")
            return
        End If

        if (Not job.ContainsKey("thumbnail_url")) Then
            Console.WriteLine("ERROR: received job does not include thumbnail_url.")
            return
        End If

        if (ffmpeg.Length = 0 or not File.Exists(ffmpeg))
            Console.WriteLine("> ffmpeg is required")
            Return
        End If

        Dim filename as String =
                job.Item("video_url").toString().Substring(
                    job.Item("video_url").toString().LastIndexOf("/", StringComparison.Ordinal) + 1).Split("?")(0)

        Console.WriteLine("> Job Details: ")
        Console.WriteLine("> Permlink: " & job.Item("permlink").ToString())
        Console.WriteLine("> URL: " & job.Item("video_url").ToString().Split("?")(0))
        Console.WriteLine("> Filename: " & filename)
        Console.WriteLine("> Hardware Acceleration: " & hwaccel)
        
        Dim dl as Video = new Video()
        
        dl.Download(job.Item("video_url").ToString(), job.Item("permlink").ToString(), filename)
        dl.Download(job.Item("thumbnail_url").ToString(), job.Item("permlink").ToString() + "/thumbnails", "default.png")

        Console.WriteLine("> Download Complete")
        Dim encoder as FFMPEG = New FFMPEG(job.Item("permlink").ToString())
        encoder.EncodePrepared(filename, ffmpeg, hwaccel)
        
    End Sub
End Module
