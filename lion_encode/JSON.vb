Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
imports System.Net.Http
imports System.IO
Imports System.Net
Imports System.Text

Public Module JSON
    
    Public Function ParseUrl(ByVal url as String) as JObject
        Console.WriteLine("> Requesting Job")
        Using client as new WebClient()
            Dim buffer as Byte() = client.DownloadData(url)
            Dim json as JObject = ParseJob(Encoding.ASCII.GetString(buffer))
            return json
        End Using
    End Function
    Public Function ParseJob(ByRef data as String) as JObject
        
        Dim serializer as JsonSerializer = New JsonSerializer()
        Dim reader as StringReader = New StringReader(data)
        dim jsonreader as JsonTextReader = New JsonTextReader(reader)
        Dim _json as JObject = CType(serializer.Deserialize(jsonreader), JObject)
        return _json
        
    End Function
    
End Module