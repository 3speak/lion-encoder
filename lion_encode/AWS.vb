Imports System.IO
Imports System.Net
Imports Amazon.S3
Imports Amazon.S3.Transfer

Public Module AWS
    Dim WithEvents transfer as TransferUtility
    Dim WithEvents config as TransferUtilityUploadDirectoryRequest
    Dim ReadOnly Files as New List(Of String)()

    Public Sub UploadVideo(permlink as String)
        Dim config as Config = New Config(GetArg("config"))
        
        Dim bucketName = config.GetItem("s3Bucket")
        Dim s3Config = New AmazonS3Config()
        s3Config.ServiceURL = config.GetItem("s3ServiceUrl")
        s3Config.ForcePathStyle = False

        Dim s3Client = New AmazonS3Client(config.GetItem("s3AccessKey"),
                                          config.GetItem("s3AccessSecret"), s3Config)

        UploadDirAsync(s3Client, permlink, bucketName).Wait()
    End Sub

    Public Sub DeleteDirectory(path As String)
        If Directory.Exists(path) Then
            'Delete all files from the Directory
            For Each filepath As String In Directory.GetFiles(path)
                File.Delete(filepath)
            Next
            'Delete all child Directories
            For Each dir As String In Directory.GetDirectories(path)
                DeleteDirectory(dir)
            Next
            'Delete a Directory
            Directory.Delete(path)
        End If
    End Sub

    Private Async Function UploadDirAsync(client as AmazonS3Client, permlink as String, bucket as string) As Task
        Dim appconfig as Config = New Config(GetArg("config"))
        Try

            transfer = New TransferUtility(client)

            config = New TransferUtilityUploadDirectoryRequest()
            config.BucketName = bucket
            config.Directory = permlink
            config.KeyPrefix = permlink
            config.SearchOption = SearchOption.AllDirectories
            config.SearchPattern = "*"
            config.CannedACL = S3CannedACL.PublicRead

            await transfer.UploadDirectoryAsync(config)

            Console.WriteLine("> Upload completed")

            DeleteDirectory(permlink)

            Console.WriteLine("> Deleted output files from local harddrive")

            Dim result = JSON.ParseUrl(appconfig.GetItem("jobFinishUrl") + "&permlink=" + permlink)

            Console.WriteLine("DONE")

        Catch ex as Exception
            console.WriteLine(ex.message)
        End Try
    End Function

    Private Sub OnProgress(sender as Object, data as UploadDirectoryProgressArgs) _
        Handles config.UploadDirectoryProgressEvent
        Dim progress as Double = Math.Round(data.NumberOfFilesUploaded/data.TotalNumberOfFiles*100, 2)
        Dim file as String = data.CurrentFile.Substring(data.CurrentFile.LastIndexOf("/", StringComparison.Ordinal) + 1)

        If not Files.Contains(data.CurrentFile) Then
            Files.Add(data.CurrentFile)
            Console.WriteLine("Uploading: " & data.CurrentFile & " :: " & progress & "%")
        End If
    End Sub
End Module