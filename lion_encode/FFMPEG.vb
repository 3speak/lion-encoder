Imports System.IO
Imports System.Net
Imports System.Text.RegularExpressions
Imports System.Threading
Imports Microsoft.VisualBasic.FileIO

Public Class FFMPEG
    private permlink as string
    private WithEvents proc as new Process()
    private WithEvents prepare as new Process()
    private config as Config

    Public Sub New(ByRef permlink as String)
        Me.permlink = permlink
        Me.config = New Config(GetArg("config"))
    End Sub

    Public Sub EncodePrepared(ByVal input as String, ByVal ffmpeg as String, ByVal hwaccel as String)
        If (Not System.IO.Directory.Exists(permlink))
            System.IO.Directory.CreateDirectory(permlink)
        End If


        Dim parameters as new ProcessStartInfo()
        parameters.FileName = ffmpeg
        parameters.Arguments = "-i " + permlink + "/" + input + " -vcodec h264 -acodec mp2 " + permlink +
                               "/pre_encoded.mp4"

        parameters.RedirectStandardError = True
        parameters.RedirectStandardOutput = True
        parameters.UseShellExecute = FALSE
        parameters.WindowStyle = ProcessWindowStyle.Hidden
        me.prepare.StartInfo = parameters
        Me.prepare.Start()

        Me.prepare.BeginOutputReadLine()
        Me.prepare.BeginErrorReadLine()
        Me.prepare.EnableRaisingEvents = True
        Me.prepare.WaitForExit()
        Dim exitCode as Integer = Me.prepare.ExitCode
        Me.prepare.Close()

        If (exitCode = 0) Then
            Encode("pre_encoded.mp4", ffmpeg, hwaccel)
        Else
            Dim result = JSON.ParseUrl(config.GetItem("jobFailUrl") + "&permlink=" + permlink)
            AWS.DeleteDirectory(permlink)
        End If
    End Sub

    Public Sub Encode(ByVal input as String, ByRef ffmpeg as String, ByVal hwaccel as String)

        If (Not System.IO.Directory.Exists(permlink))
            System.IO.Directory.CreateDirectory(permlink)
        End If

        If (Not System.IO.Directory.Exists(permlink & "/720p"))
            System.IO.Directory.CreateDirectory(permlink & "/720p")
        End If
        If (Not System.IO.Directory.Exists(permlink & "/1080p"))
            System.IO.Directory.CreateDirectory(permlink & "/1080p")
        End If
        If (Not System.IO.Directory.Exists(permlink & "/480p"))
            System.IO.Directory.CreateDirectory(permlink & "/480p")
        End If
        If (Not System.IO.Directory.Exists(permlink & "/360p"))
            System.IO.Directory.CreateDirectory(permlink & "/360p")
        End If
        Dim parameters as new ProcessStartInfo()
        parameters.FileName = ffmpeg
        If hwaccel = "True" Then
            parameters.Arguments = "-hwaccel cuvid -c:v h264_cuvid  -threads 20 -i " & permlink &
                                   "/" & input &
                                   "  \
  -vf scale_npp=640:360 -c:a aac -ar 48000 -c:v h264_nvenc -profile:v main  -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_list_size 0  -b:v:0 800k -maxrate 856k -bufsize 1200k -b:a 96k -hls_segment_filename " &
                                   permlink & "/360p/%03d.ts " & permlink &
                                   "/360p.m3u8 \
   -vf scale_npp=842:480 -c:a aac -ar 48000 -c:v h264_nvenc -profile:v main  -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_list_size 0 -b:v:1 1400k -maxrate 1498k -bufsize 2100k -b:a 128k -hls_segment_filename " &
                                   permlink & "/480p/%03d.ts " & permlink &
                                   "/480p.m3u8 \
  -vf scale_npp=1280:720 -c:a aac -ar 48000 -c:v h264_nvenc -profile:v main  -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_list_size 0 -b:v:2 2800k -maxrate 2996k -bufsize 4200k -b:a 128k -hls_segment_filename " &
                                   permlink & "/720p/%03d.ts " & permlink &
                                   "/720p.m3u8 \
  -vf scale_npp=1920:1080 -c:a aac -ar 48000 -c:v h264_nvenc -profile:v main  -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_list_size 0 -b:v:3 5000k -maxrate 5350k -bufsize 7500k -b:a 192k -hls_segment_filename " &
                                   permlink & "/1080p/%03d.ts " & permlink & "/1080p.m3u8"
            Console.WriteLine("ffmpeg " & parameters.Arguments)
        Else
            parameters.Arguments = "-threads 12 -i " & permlink &
                                   "/" & input &
                                   " -preset ultrafast    -map 0:v -map 0:a \
  -vf scale=w=640:h=360:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod  -b:v:0 800k -maxrate 856k -bufsize 1200k -b:a 96k -hls_segment_filename " &
                                   permlink & "/360p/%03d.ts " & permlink &
                                   "/360p.m3u8 \
   -vf scale=w=842:h=480:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v:1 1400k -maxrate 1498k -bufsize 2100k -b:a 128k -hls_segment_filename " &
                                   permlink & "/480p/%03d.ts " & permlink &
                                   "/480p.m3u8 \
  -vf scale=w=1280:h=720:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v:2 2800k -maxrate 2996k -bufsize 4200k -b:a 128k -hls_segment_filename " &
                                   permlink & "/720p/%03d.ts " & permlink &
                                   "/720p.m3u8 \
  -vf scale=w=1920:h=1080:force_original_aspect_ratio=decrease -c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod -b:v:3 5000k -maxrate 5350k -bufsize 7500k -b:a 192k -hls_segment_filename " &
                                   permlink & "/1080p/%03d.ts " & permlink & "/1080p.m3u8"

        End If

        parameters.RedirectStandardError = True
        parameters.RedirectStandardOutput = True
        parameters.UseShellExecute = FALSE
        parameters.WindowStyle = ProcessWindowStyle.Hidden
        me.proc.StartInfo = parameters
        Me.proc.Start()

        Me.proc.BeginOutputReadLine()
        Me.proc.BeginErrorReadLine()

        Me.proc.EnableRaisingEvents = True
        Me.proc.WaitForExit()
        Dim exitCode as Integer = Me.proc.ExitCode
        If (exitCode = 0) Then
            dim output as String = Me.permlink & "/default.m3u8"

            System.IO.File.Create(output).Dispose()

            Dim line as [String]() = {"#EXTM3U",
                                      "#EXT-X-VERSION:3",
                                      "#EXT-X-STREAM-INF:BANDWIDTH=420000,RESOLUTION=640x360",
                                      "360p.m3u8",
                                      "#EXT-X-STREAM-INF:BANDWIDTH=680000,RESOLUTION=842x480",
                                      "480p.m3u8",
                                      "#EXT-X-STREAM-INF:BANDWIDTH=1256000,RESOLUTION=1280x720",
                                      "720p.m3u8",
                                      "#EXT-X-STREAM-INF:BANDWIDTH=2000000,RESOLUTION=1920x1080",
                                      "1080p.m3u8"
                                     }

            Dim master = String.Join(Environment.NewLine, line.ToArray())
            File.WriteAllText(output, master)
            Console.WriteLine()

            File.WriteAllText(Me.permlink & "/1080p.m3u8",
                              Regex.Replace(File.ReadAllText(Me.permlink & "/1080p.m3u8"), "(\d+.ts)", "1080p/$1"))
            File.WriteAllText(Me.permlink & "/720p.m3u8",
                              Regex.Replace(File.ReadAllText(Me.permlink & "/720p.m3u8"), "(\d+.ts)", "720p/$1"))
            File.WriteAllText(Me.permlink & "/480p.m3u8",
                              Regex.Replace(File.ReadAllText(Me.permlink & "/480p.m3u8"), "(\d+.ts)", "480p/$1"))
            File.WriteAllText(Me.permlink & "/360p.m3u8",
                              Regex.Replace(File.ReadAllText(Me.permlink & "/360p.m3u8"), "(\d+.ts)", "360p/$1"))


            AWS.UploadVideo(Me.permlink)
        Else
            Dim result = JSON.ParseUrl(config.GetItem("jobFailUrl") + "&permlink=" + permlink)
            AWS.DeleteDirectory(permlink)
        End If
        Me.proc.Close()
    End Sub


    Private Sub onDataOutput(sendingProcess as Object, outLine as DataReceivedEventArgs) _
        Handles proc.OutputDataReceived, proc.ErrorDataReceived, prepare.OutputDataReceived, prepare.ErrorDataReceived
        Try
            Console.WriteLine(outLine.Data.ToString())
        Catch

        End Try
    End Sub
End Class
