Imports System.IO
Imports Newtonsoft.Json.Linq

Public Class Config
    Private configJson as JObject

    Public Sub New(ByVal configFilePath as String)
        if (File.Exists(configFilePath))
            Console.WriteLine("> Parsing config file: " + configFilePath)
            dim configString as String = File.ReadAllText(configFilePath)
            configJson = JSON.ParseJob(configString)
        Else
            Console.WriteLine("!! Config file \""" + configFilePath + "\"" does not exist.")
        End If
    End Sub
    
    Public Function GetItem(ByVal item as string) As string
        return configJson.Item(item)
    End Function
    
End Class