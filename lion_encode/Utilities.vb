Public Module Utilities
    Public Sub ParseArgs()
        
    End Sub
    
    Public Function GetArg(ByVal name as String) 
        For each arg as String in Environment.GetCommandLineArgs()
            If (arg.StartsWith("--")) Then
                if (arg.Contains("=")) Then
                    
                    Dim kv as String = arg.Substring(2)
                    Dim key as String = kv.Substring(0, kv.IndexOf("=", StringComparison.Ordinal))
                    if (key = name) Then
                        return kv.Substring(kv.IndexOf("=", StringComparison.Ordinal) +1)
                    End If
                End If
            End If
        Next
        Return ""
    End Function
End Module